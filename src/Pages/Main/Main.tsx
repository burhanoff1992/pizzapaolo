import "./Main.css";
import DataGrid, {Column, textEditor} from "react-data-grid";
import {Component, useState} from "react";
import 'react-data-grid/lib/styles.css';
import EmployeeSelect from "./EmployeeSelect";
import {DatePicker, Divider} from "antd";

const {RangePicker} = DatePicker;

export interface Row {
    date: string,
    employee_id: string,
    facility_id: string,
    duration: number,
}

function createRows(): Row[] {
    const rows: Row[] = [];

    for (let i = 0; i < 7; i++) {
        rows.push({
            date: `${i + 1}.1.2023`,
            employee_id: "Конь Павлов",
            facility_id: "Точка №" + i,
            duration: 12,
        });
    }

    return rows;
}

const Main = () => {
    const [rows, setRows] = useState(createRows);
    const [datesRange, setDatesRange] = useState(7);

    const columns: Column<Row>[] = [
        {
            key: "date",
            name: "Дата",
            width: 120,
            frozen: true,
            editable: false,
        },
        {
            key: "employee_id",
            name: "Сотрудник",
            width: 150,
            resizable: true,
            editor: EmployeeSelect
        },
        {
            key: "facility_id",
            name: "Точка",
            width: 150,
            resizable: true,
            editor: textEditor,
        },
        {
            key: "duration",
            name: "Длительность",
            width: 150,
            resizable: true,
        },
    ];

    return (
        <div>
            <RangePicker />

            <Divider />

            <DataGrid
                className="rdg-light"
                columns={columns}
                rows={rows}
                onRowsChange={(e) => {
                    setRows(e),
                    console.log(e)
                }}
            />
        </div>
    );
};

export default Main;