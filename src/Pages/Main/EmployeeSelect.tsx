import {EditorProps} from "react-data-grid";
import {Row} from "./Main";

const employees = ["Ильдар Бурханов", "Павел Конин", "Конь Павлов"] as const;

const EmployeeSelect = ({ row, onRowChange }: EditorProps<Row>) => {
    return (
        <select
            className="grid-select"
            value={row.employee_id}
            onChange={(event) => onRowChange({ ...row, employee_id: event.target.value }, true)}
            autoFocus
        >
            {employees.map((employee) => (
                <option key={employee} value={employee}>
                    {employee}
                </option>
            ))}
        </select>
    );
};

export default EmployeeSelect;