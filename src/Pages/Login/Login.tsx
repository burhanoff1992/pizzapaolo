import React, {Dispatch, SetStateAction} from 'react';
import "./Login.css";
import {Button, Divider, Form, Input, notification} from "antd";

interface LoginProps {
    setAuthorized: Dispatch<SetStateAction<boolean>>
}

const Login: React.FC<LoginProps> = (props: LoginProps) => {
    const [formLogin] = Form.useForm();

    const onFinish = () => {
        console.log(formLogin.getFieldsValue());

        props.setAuthorized(true);

        if (Math.round(Math.random())) {
            notification.success({
                message: "Авторизация",
                description: "Успешний вход",
                placement: "bottom"
            });
        } else {
            notification.error({
                message: "Авторизация",
                description: "Неверный логин или пароль",
                placement: "bottom"
            });
        }
    }

    const inputRules = [
        {
            required: true,
            message: "Поле не должно быть пустым"
        }
    ];

    return (
        <div className="login" style={{backgroundImage: `url(/pizza_bg.jpg)`}}>
            <div className="login-overlay">
                <div className="login-inner">
                    <div className="login-block">
                        <div className="login-head">
                            <div className="login-logo">
                                <img src="/logo.svg" alt="Система видеонаблюдения &laquo;Пицца Паоло&raquo;" />
                            </div>

                            <h1><i className="fa-solid fa-camera" /> Система видеонаблюдения</h1>
                        </div>

                        <div className="login-form">
                            <Divider style={{color: "#777", fontWeight: 400, fontSize: 14, marginTop: 0}}>
                                Вход в систему
                            </Divider>

                            <Form form={formLogin} onFinish={onFinish}>
                                <Form.Item name="login" rules={inputRules}>
                                    <Input
                                        placeholder="Логин"
                                        prefix={(
                                            <span className="anticon login-icon">
                                        <i className="fa-solid fa-user"/>
                                    </span>
                                        )}
                                    />
                                </Form.Item>
                                <Form.Item name="password" rules={inputRules}>
                                    <Input
                                        placeholder="Пароль"
                                        prefix={(
                                            <span className="anticon login-icon">
                                        <i className="fa-solid fa-lock"/>
                                    </span>
                                        )}
                                    />
                                </Form.Item>
                                <Form.Item style={{marginBottom: 0}}>
                                    <Button
                                        type="primary"
                                        htmlType="submit"
                                        size="large"
                                        icon={(
                                            <span className="anticon">
                                            <i className="fa-solid fa-arrow-right-to-bracket" />
                                        </span>
                                        )}
                                        block
                                    >
                                        Войти
                                    </Button>
                                </Form.Item>
                            </Form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default Login;