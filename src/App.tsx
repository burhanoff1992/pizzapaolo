import React, {useState} from "react";
import {
    BrowserRouter as Router, Link, NavLink,
    Route, Routes
} from "react-router-dom";
import Login from "./Pages/Login/Login";
import Main from "./Pages/Main/Main";
import {Breadcrumb, Layout, Menu, theme} from "antd";
import {UserOutlined} from "@ant-design/icons";
import 'ag-grid-community/styles/ag-grid.css';
import 'ag-grid-community/styles/ag-theme-alpine.css';

const {Header, Content, Footer, Sider} = Layout;

const App = () => {
    const {
        token: {colorBgContainer},
    } = theme.useToken();

    const [authorized, setAuthorized] = useState(true);

    return authorized ? (
        <Router>
            <Layout>
                <Header className="header">
                    <div className="logo" style={{backgroundImage: `url(/logo.svg)`}} />
                </Header>
                <Content style={{ padding: '0 50px' }}>
                    <Breadcrumb style={{ margin: '16px 0' }}>
                        <Breadcrumb.Item>Главная</Breadcrumb.Item>
                    </Breadcrumb>

                    <Layout style={{
                        padding: '24px 0',
                        background: colorBgContainer,
                    }}>
                        <Sider style={{ background: colorBgContainer }} width={240}>
                            <Menu
                                mode="inline"
                                style={{ height: '100%' }}
                            >
                                <Menu.Item icon={<UserOutlined />} key={1}>
                                    <NavLink to={"/"}>Главная</NavLink>
                                </Menu.Item>
                            </Menu>
                        </Sider>

                        <Content style={{ padding: '0 24px', minHeight: 280 }}>
                            <Routes>
                                <Route path="/" element={<Main/>}/>
                            </Routes>
                        </Content>
                    </Layout>
                </Content>

                <Footer style={{ textAlign: 'center' }}>
                    Система видеонаблюдения &laquo;Пицца Паоло&raquo;
                </Footer>
            </Layout>
        </Router>
    ) : (
        <Router>
            <Routes>
                <Route path="/" element={<Login setAuthorized={setAuthorized} />}/>
            </Routes>
        </Router>
    )
};

export default App;