import React from 'react';
import ReactDOM from 'react-dom/client';
import {ConfigProvider, DatePicker, ThemeConfig} from "antd";
import 'antd/dist/reset.css';
import './main.css';

import 'moment/locale/ru';
import Main from "./Pages/Main/Main";
import App from "./App";

const colorPrimary = "#8327d9";

const theme: ThemeConfig = {
    token: {
        colorPrimary: colorPrimary,
    },
    components: {
        Layout: {
            colorBgHeader: "#53495d"
        }
    }
}

ReactDOM.createRoot(document.getElementById('root') as HTMLElement).render(
    <ConfigProvider theme={theme}>
        <App />
    </ConfigProvider>
);
